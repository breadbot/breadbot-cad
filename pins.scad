pin_width = 0.69;
pin_height = 4;
pin_spacing = 2.54;

module pin()
     translate([0,0,-0.1]) cube([pin_width, pin_width, pin_height + 0.1]);


module pins(w, h)
     translate([(pin_spacing - pin_width) / 2, (pin_spacing - pin_width) / 2])
     for (r = [0:1:w-1])
          for (c = [0:1:h-1])
               translate([r * pin_spacing, c * pin_spacing]) pin();
