include <constants.scad>

camera_width = 25;
camera_height = 24;
camera_thickness = 1;
camera_support_thickness = 5;
camera_support_height = 15;
camera_bottom_padding = 5;
camera_indent = 1;
camera_mount_width = camera_width + camera_support_thickness * 2;

module cameraMount() {
     difference() {
          cube([camera_width + (camera_support_thickness - camera_indent) * 2, camera_support_thickness, camera_support_height]);
          translate([camera_support_thickness, -margin, -margin])
               cube([camera_width - camera_indent * 2, camera_support_thickness + margin * 2, camera_support_height + 2 * margin]);
          translate([camera_support_thickness - camera_indent, (camera_support_thickness - camera_thickness) / 2, camera_bottom_padding])
               cube([camera_width, camera_thickness, camera_support_height - camera_bottom_padding]);
     }
}
