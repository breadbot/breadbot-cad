include <constants.scad>

breadbar_width = 60;
breadboard_width = 54.7;
breadbar_column_height = 6;
breadbar_base_thickness = 4;
breadbar_depth = 5;
breadbar_trapezoid_height=1.5;
breadbar_trapezoid_b1 = 3.8;
breadbar_trapezoid_b2 = 3.2;

module trapezoid(b1, b2, height, thickness) {
     polyhedron(
          [[0, 0, 0],
           [(b2 - b1) / 2, height, 0],
           [(b2 + b1) / 2, height, 0],
           [b2, 0, 0],
           [0, 0, thickness],
           [(b2 - b1) / 2, height, thickness],
           [(b2 + b1) / 2, height, thickness],
           [b2, 0, thickness]],

          [[0, 3, 2, 1],
           [4, 5, 6, 7],
           [1, 2, 6, 5],
           [0, 1, 5, 4],
           [0, 4, 7, 3],
           [3, 7, 6, 2]]
          );
}

module breadbar() {
     difference()
     {
          cube([breadbar_width, breadbar_depth, breadbar_base_thickness + breadbar_column_height]);
          translate([(breadbar_width - breadboard_width) / 2, -margin, breadbar_base_thickness])
               cube([breadboard_width, breadbar_depth + 2 * margin, breadbar_column_height + margin]);

          translate([breadbar_width - (breadbar_width - breadboard_width) / 2 + breadbar_trapezoid_height,
                     (breadbar_depth - breadbar_trapezoid_b2) / 2,
                     10 - breadbar_base_thickness])
               rotate([0, 0, 90])
               trapezoid(breadbar_trapezoid_b1, breadbar_trapezoid_b2, breadbar_trapezoid_height, breadbar_column_height + margin);

          translate([breadbar_width - (breadbar_width - breadboard_width) / 2 - margin,
                     (breadbar_depth - breadbar_trapezoid_b1) / 2,
                     breadbar_base_thickness])
               cube([margin * 2, breadbar_trapezoid_b1, breadbar_column_height + margin * 2]);
     }

     translate([(breadbar_width - breadboard_width) / 2 + breadbar_trapezoid_height,
                (breadbar_depth - breadbar_trapezoid_b2) / 2,
                breadbar_base_thickness])
          rotate([0, 0, 90])
          trapezoid(breadbar_trapezoid_b1, breadbar_trapezoid_b2, breadbar_trapezoid_height, breadbar_column_height);

}
