COLORSCHEME=DeepOcean
default: stl res

stl: stl/battery_mount.stl stl/axle.stl

res: res/battery_mount.png res/axle.png

stl/battery_mount.stl: battery_mount_export.scad battery_mount.scad pins.scad camera_mount.scad constants.scad
	mkdir -p stl
	openscad -o $@ $<

stl/axle.stl: axle_export.scad axle.scad breadbar.scad constants.scad
	mkdir -p stl
	openscad -o $@ $<

res/battery_mount.png: battery_mount_export.scad battery_mount.scad pins.scad camera_mount.scad constants.scad
	mkdir -p res
	openscad --camera=0,0,0,85,0,15,0 --viewall --autocenter --render --colorscheme=$(COLORSCHEME) --imgsize=1920,1080 -o $@ $<

res/axle.png: axle_export.scad axle.scad breadbar.scad constants.scad
	mkdir -p res
	openscad --camera=0,0,0,85,0,15,0 --viewall --autocenter --render --colorscheme=$(COLORSCHEME) --imgsize=1920,1080 -o $@ $<

clean:
	rm -rf stl res
