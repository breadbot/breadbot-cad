include <breadbar.scad>;
include <constants.scad>

axle_length = 100;
axle_hole_spaceing = 17.5;
axle_hole_r = 1.5;
axle_height = 25;
axle_depth = breadbar_depth;
axle_outer_thickness = 2.5;
axle_long_thickness = 5;
axle_breadbar_spacing = 5;
axle_spool_width = 20;

module axle() {
     difference()
     {
          cube([axle_length, axle_depth, axle_height]);
          translate([axle_outer_thickness, -margin, -margin])
               cube([axle_length - 2 * axle_outer_thickness, axle_depth + margin * 2, (axle_height - axle_long_thickness) / 2 + margin]);
          translate([axle_outer_thickness, -margin, axle_height - 10])
               cube([axle_length - 2 * axle_outer_thickness, axle_depth + margin * 2, (axle_height - axle_long_thickness) / 2 + margin]);

          translate([-margin, axle_outer_thickness, (axle_height - axle_hole_spaceing) / 2])
               rotate([0, 90])
               cylinder(axle_length + 2 * margin, axle_hole_r, axle_hole_r, $fn=100);

          translate([-margin, axle_outer_thickness, (axle_height + axle_hole_spaceing) / 2])
               rotate([0, 90])
               cylinder(axle_length + 2 * margin, axle_hole_r, axle_hole_r, $fn=100);

     }

     translate([(axle_length - breadbar_width) / 2,
                0,
                (axle_height + axle_long_thickness) / 2 + axle_breadbar_spacing])
          breadbar();
     translate([(axle_length - axle_spool_width) / 2, 0, (axle_height + axle_long_thickness) / 2])
          cube([axle_spool_width, axle_depth, axle_depth]);

}
