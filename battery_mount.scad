include <pins.scad>
include <camera_mount.scad>

battery_length = 73;
battery_width = 72;
battery_support_height = 15;
battery_support_width = 5;
battery_mount_thickness = 5;
battery_mount_leg_spacing_length = 67.8;
battery_mount_leg_spacing_width = 18 * pin_spacing;
battery_mount_leg_height = 20;

module support() {
     cube([battery_support_width + battery_mount_thickness,
           battery_mount_thickness,
           battery_mount_thickness + battery_support_height]);
     cube([battery_mount_thickness,
           battery_support_width + battery_mount_thickness,
           battery_mount_thickness + battery_support_height]);
}

module battery_mount() {
     cube([battery_length + battery_mount_thickness * 2, battery_width + battery_mount_thickness * 2, battery_mount_thickness]);
     support();
     translate([battery_length + 2 * battery_mount_thickness, 0, 0])
          rotate([0, 0, 90])
          support();

     translate([0, battery_width + 2 * battery_mount_thickness, 0])
          rotate([0, 0, -90])
          support();

     translate([battery_length + 2 * battery_mount_thickness, battery_width + 2 * battery_mount_thickness, 0])
          rotate([0, 0, 180])
          support();

     pillar_width_translation = (battery_width  - battery_mount_leg_spacing_width) / 2 + battery_mount_thickness - pin_spacing;
     pillar_length_translation = (battery_length - battery_mount_leg_spacing_length) / 2 + battery_mount_thickness - pin_spacing;

     translate([pillar_length_translation, pillar_width_translation, 0])
          breadboardPillar();

     translate([battery_mount_leg_spacing_length + pillar_length_translation, pillar_width_translation, 0])
          breadboardPillar();

     translate([pillar_length_translation, battery_mount_leg_spacing_width + pillar_width_translation, 0])
          breadboardPillar();

     translate([pillar_length_translation + battery_mount_leg_spacing_length, 18 * pin_spacing + pillar_width_translation, 0])
          breadboardPillar();

     translate([camera_support_thickness, (battery_width + 2 * battery_mount_thickness - camera_mount_width) / 2, battery_mount_thickness])
          rotate([0, 0, 90])
          cameraMount();
}

module breadboardPillar() {
     translate([0, 0, -20]) {
          difference() {
               cube([2*pin_spacing, 2*pin_spacing, battery_mount_leg_height]);
               pins(2, 2);
          }
     }
}

battery_mount();
